﻿using UnityEngine;

public class FollowBy : MonoBehaviour {
    
    public GameObject leader;

    private Vector3 offset;

    private void Start() {
        offset = transform.position - leader.transform.position;
    }

    private void LateUpdate() {
        transform.position = leader.transform.position + offset;
    }
}