﻿using UnityEngine;

public class PickUpMagnet : MonoBehaviour {
    
    public float hoverForce;

    private void OnTriggerStay(Collider other) {
        if (other.gameObject.CompareTag("Pick Up"))
            other.attachedRigidbody.AddForce(Vector3.up * hoverForce, ForceMode.Acceleration);
    }
}